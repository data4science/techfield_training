1. Import the numpy package under the name np 
import numpy as np
2. Print the numpy version and the configuration
print np.version.version
print np.show_config()
3. Create a null vector of size 10
null_vector = np.zeros(10)
4. How to find the memory size of any array 
any_array.size * any_array.itemsize
5. How to get the documentation of the numpy add function from the command line?
np.info(np.add)
6. Create a null vector of size 10 but the fifth value which is 1
null_vector[4] = 1
7. Create a vector with values ranging from 10 to 49
np.arange(10, 50)
8. Reverse a vector (first element becomes last)
any_array[-1::-1]
9. Create a 3x3 matrix with values ranging from 0 to 8
np.reshape(np.arange(0,8), (3,3))
10. Find indices of non-zero elements from [1,2,0,0,4,0]
np.nonzero()
11. Create a 3x3 identity matrix
np.identity(3)
12. Create a 3x3x3 array with random values
np.random.random((3,3,3))
13. Create a 10x10 array with random values and find the minimum and maximum values
np.minimum(np.random.random((10,10)))
np.maximum(np.random.random((10,10)))
14. Create a random vector of size 30 and find the mean value
np.random.random(30).mean()
15. Create a 2d array with 1 on the border and 0 inside
np.ones((10,10))[1:-1,1:-1] = 0
16. How to add a border (filled with 0's) around an existing array?

17. What is the result of the following expression?

0 * np.nan			
np.nan == np.nan	
np.inf > np.nan		
np.nan - np.nan
0.3 == 3 * 0.1

18. Create a 5x5 matrix with values 1,2,3,4 just below the diagonal
Z = np.zeros((8,8),dtype=int)
Z[1::2,::2] = 1
Z[::2,1::2] = 1
19. Create a 8x8 matrix and fill it with a checkerboard pattern
np.unravel_index(100,(6,7,8))
20. Consider a (6,7,8) shape array, what is the index (x,y,z) of the 100th element?

21. Create a checkerboard 8x8 matrix using the tile function

22. Normalize a 5x5 random matrix

23. Create a custom dtype that describes a color as four unsigned bytes (RGBA)

24. Multiply a 5x3 matrix by a 3x2 matrix (real matrix product)

25. Given a 1D array, negate all elements which are between 3 and 8, in place.

26. What is the output of the following script?

print(sum(range(5),-1))
from numpy import *
print(sum(range(5),-1))

27. Consider an integer vector Z, which of these expressions are legal? (★☆☆)
Z**Z
2 << Z >> 2
Z <- Z
1j*Z
Z/1/1
Z<Z>Z

28. What are the result of the following expressions?
np.array(0) / np.array(0)
np.array(0) // np.array(0)
np.array([np.nan]).astype(int).astype(float)

29. How to round away from zero a float array ?

30. How to find common values between two arrays?

31. How to ignore all numpy warnings (not recommended)?

32. Is the following expressions true?

np.sqrt(-1) == np.emath.sqrt(-1)

33. How to get the dates of yesterday, today and tomorrow?

34. How to get all the dates corresponding to the month of July 2016?

35. How to compute ((A+B)*(-A/2)) in place (without copy)?

36. Extract the integer part of a random array using 5 different methods

37. Create a 5x5 matrix with row values ranging from 0 to 4

38. Consider a generator function that generates 10 integers and use it to build an array

39. Create a vector of size 10 with values ranging from 0 to 1, both excluded

40. Create a random vector of size 10 and sort it

41. How to sum a small array faster than np.sum?

42. Consider two random array A and B, check if they are equal

43. Make an array immutable (read-only)

44. Consider a random 10x2 matrix representing cartesian coordinates, convert them to polar coordinates

45. Create random vector of size 10 and replace the maximum value by 0

46. Create a structured array with x and y coordinates covering the [0,1]x[0,1] area

47. Given two arrays, X and Y, construct the Cauchy matrix C (Cij =1/(xi - yj))

48. Print the minimum and maximum representable value for each numpy scalar type

49. How to print all the values of an array?

50. How to find the closest value (to a given scalar) in a vector?