def indexer(aList):
    '''
    Takes a list (aList), and returns a list of equal length where values are replaced with index values.
    '''
    index = 0
    index_list = []
    for element in aList:
        index_list += [index]
        index += 1
    return index_list

def zipper(list1, list2):
    '''
    Takes two lists (list1, list2) of equal length, and returns a list of tuples.
    Equivalent to list(zip(list1, list2)).
    '''
    zipped_list = []
    indexed1 = indexer(list1)
    indexed2 = indexer(list2)
    for element in indexed1:
            zipped_list += [(list1[element], list2[element])]
    return zipped_list

def manipulate_matrix(matrix):
    '''
    Takes a two dimensional list of lists (matrix) where the length of all lists are equal, and returns a list of
    lists with the values shuffled such that matrix is rotated 90 degrees counter clockwise.
    '''
    rows_indexed = indexer(matrix)
    zipped = zipper(rows_indexed, matrix)
    return [[matrix[column_index][row_index] for column_index in indexer(row)][-1::-1] for row_index, row in zipped]

eg = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

print manipulate_matrix(eg)
[[7, 4, 1], [8, 5, 2], [9, 6, 3]]

