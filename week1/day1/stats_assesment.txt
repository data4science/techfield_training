Null hypothesis:
An average box of cereal contains 368 g of cereal.

Alternate hypothesis:
An average box of cereal contains less than 368 g of cereal.

372.5 - 368 = 4.5

sigma = 15

15 / sqrt(25) = 15 / 5 = 3

2 * 3 = 6

4.5 < 6

Here we use the one-sided z-test because we don't want customers or customer advocacy groups to retaliate for selling them too little cereal.  We fail to reject the null hypothesis because the difference between the sample mean, and the desired weight is less than two standard deviations.