

'''
1. Prove through code what happens to the volume of a sphere in higher dimensions. What about a cube?

2. Is my assertion that in high-dimensions every distance is about the same, and none is near or far, correct? Why or why not?

3. Why does any of this matter in terms of data science / machine learning?

Bonus: Answer all the questions were possible with a mathematical proof not just code.
'''

import numpy as np
import matpot