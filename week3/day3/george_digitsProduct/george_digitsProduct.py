'''
Given an integer product, find the smallest positive (i.e. greater than 0)
integer the product of whose digits is equal to product. If there is no such
integer, return -1 instead.

Example

For product = 12, the output should be
digitsProduct(product) = 26;
For product = 19, the output should be
digitsProduct(product) = -1.
Input/Output

[execution time limit] 4 seconds (py3)

[input] integer product

Guaranteed constraints:
0 ≤ product ≤ 600.

[output] integer
'''

def digitsProduct(product): 
    #Lower than 10
    if product < 10:
        if product == 0:
            return 10
        if product % 2 == 0 and product != 2:
        	return 2 * 10 + product / 2
        if product == 9:
        	return 33
        return product
    return int(myRecusiveFunction(product))

#Todo -> fix me :(

#what does this function even return?
#returns just a list 
def myRecusiveFunction(semiProduct, aStr=''):
	for i in range(2,10):
		if semiProduct % i != 0:
			return -1
		if semiProduct % i == 0:
			aStr += str(i)
			semiProduct /= i
			myRecusiveFunction(semiProduct, aStr)
		return aStr


print(digitsProduct(12) == 26)
print(digitsProduct(19) == -1)
print(digitsProduct(450) == 2559)
print(digitsProduct(0) == 10)

"""
#is the number prime
for i in range(2,9):
    if product % i == 0:
        return -1
"""

#Greater than 10
#recursion...
#aList = []
#another error here
#for i in range(2,10):
#    #you have to leave append on the next line
#    aList.append(myRecusiveFunction(i))
#    #aList is somethign liek this -> [[1,2,3],[3,42,2],[4,2],[4,2],[5,2]]

#return someInt
     


'''
you must keep the append on line 21

and use the recusive Funciton written below, on line 30

no other nested loop is allowed within the for loop on line 19

Test 1
Input:
product: 12
Output:
null
Expected Output:
26
Console Output:
Empty

Test 2
Input:
product: 19
Output:
null
Expected Output:
-1
Console Output:
Empty

Input:
product: 450
Output:
null
Expected Output:
2559
Console Output:
Empty

Test 4
Input:
product: 0
Output:
10
Expected Output:
10
Console Output:
Empty

Input:
product: 13
Output:
null
Expected Output:
-1
Console Output:
Empty

Input:
product: 1
Output:
1
Expected Output:
1
Console Output:
Empty

Test 7
Input:
product: 243
Output:
null
Expected Output:
399
Console Output:
Empty

Test 8
Input:
product: 576
Output:
null
Expected Output:
889
Console Output:
Empty

Test 9
Input:
product: 360
Output:
null
Expected Output:
589
Console Output:
Empty
'''