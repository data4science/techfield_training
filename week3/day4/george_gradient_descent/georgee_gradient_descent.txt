Why would you use batch gradient descent vs stochastic graidient descent?
****
Batch gradient descent is better for data in higher dimensions becuase it will converge exactly, and there is less chance of mistaking a local eextrema for a global extrema in higher dimensions.
Stochastic graidient descent is better in the opposite situations, but does not converge.  However, it is also less likely to mistake a local extrema for a global extrema.

When do you know to use a particular optimization algorithm.
****
Use batch when complete convergency is absolutely necessary (? not sure when that would be?).  Use stochastic when computational resources are more scarce, and the computation needs to be completed more quickly.

What is dependency injection?

Use whatever data you want ->

Compare the number of iterations it takes for each algorithm to obtain an estimate accurate to 1e-3 (you may wish to set a cap for maximum number of iterations). Which method converges to the optimal point in fewer iterations? Briefly explain why this result should be expected. 

****
With batch gradient descent converges to the optimal point in fewer iterations because it is taking the most direct path to the extrema while stocastic takes more random steps.

Compare the performance of stochastic gradient descent for the following learning rates: 1, 0.1, 0.001, 0.0001. Based on your observations, briefly describe the effect of the choice of learning rate on the performance of the algorithm.

****
Stochastic gradient desceent performs slower, but more accurately at smaller learning rates.

Bonus -> Implement gradient descent / Implement stochastic gradient descent 

