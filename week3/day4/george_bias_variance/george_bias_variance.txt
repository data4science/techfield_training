Using the make_moons dataset from sklearn -> 

from sklearn.datasets import make_moons
X, y = make_moons(
    n_samples=500,  # the number of observations
    random_state=1,
    noise=0.3
)

Answer the following questions ->

Is this a regression or classification problem?
Conceptually, how is KNN being applied to this problem to make a prediction?

****
This is a classification problem.  Conceptually, KNN returns the dominant value of samples within the k-nearest number.

What predictions would an ideal machine learning model make, with respect to a descision boundary?

****
An ideal machine learning model would make a decision boundary that is complicated enough to seperate most of the data acurately, while simple enough to generalize to various test data sets.

Change the value of K, show the error for a variety of K, more is better...
	Choose a very small value of K.
		Do you "see" low variance or high variance?
		****
		High variance.
	 	Do you "see" low bias or high bias?
	 	****
	 	Low bias.

	Repeat this with a very large value of K. 
		Do you "see" low variance or high variance?
		****
		Low variance.
		Do you "see" low bias or high bias?
		****
		High bias.

	Try using other values of K. 
		What value of K do you think is "best"? Evaluate as many K as possable 10+.
		How do you define "best"?
		****
		The best value of K balances variance and bias.  Not too much of either.

Why should we care about variance at all?
****
Models can be very biased with too low of a variance, and models with too high of a variance won't be acurate.

Shouldn't we just minimize bias and ignore variance?
****
No, ideally there is a balance between bias and variance.

Does a high value for K cause "overfitting" or "underfitting"?
****
A high value for K causes "underfitting".

Bonus -> Make those fancy plots I had in class, with the decision boundary shown, along with the points and thier respect classes shown by way of different colors.