'''
For names = ["doc", "doc", "image", "doc(1)", "doc"],
the output should be
fileNaming(names) = ["doc", "doc(1)", "image", "doc(1)(1)", "doc(2)"]
'''
'''
Input:
names: ["a(1)", "a(6)", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"]
Expected Output:
["a(1)", "a(6)", "a", "a(2)", "a(3)", "a(4)", "a(5)", "a(7)", "a(8)",
"a(9)", "a(10)", "a(11)"]
'''

"""
You are given an array of desired filenames in the order of their creation.
Since two files cannot have equal names, the one which comes later will have
an addition to its name in a form of (k), where k is the smallest positive
integer such that the obtained name is not used yet. Return an array of names
that will be given to the files.

Example For names = ["doc", "doc", "image", "doc(1)", "doc"]
fileNaming(names) = ["doc", "doc(1)", "image", "doc(1)(1)", "doc(2)"].
"""

def fileNaming(names):
	names_dict = {}
	new_names_list = []
	for name in names:
		if name in names_dict.keys():
			names_dict[name] += 1
		else:
			names_dict[name] = 1
	for key, value in names_dict.items():
		if value == 1:
			new_names_list.append(key)
		if value == 2:
			new_names_list.append(key + '(1)')
		if value > 2:
			new_names_list.append(key + '({})'.format(value - 1))
	return new_names_list

names = ["doc", "doc", "image", "doc(1)", "doc"]
new_names = ["doc", "doc(1)", "image", "doc(1)(1)", "doc(2)"]
print(fileNaming(names) == new_names)
print(fileNaming(names))

names = ["a(1)", "a(6)", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"]
new_names = ["a(1)", "a(6)", "a", "a(2)", "a(3)", "a(4)", "a(5)", "a(7)", "a(8)", 
"a(9)", "a(10)", "a(11)"]
print(fileNaming(names) == new_names)
print(fileNaming(names))

names = ["dd", "dd(1)", "dd(2)", "dd", "dd(1)", "dd(1)(2)", "dd(1)(1)", "dd",
"dd(1)"]
new_names = ["dd", "dd(1)", "dd(2)", "dd(3)", "dd(1)(1)", "dd(1)(2)", "dd(1)(1)(1)",
"dd(4)", "dd(1)(3)"]
print(fileNaming(names) == new_names)
print(fileNaming(names))

#change lines 26 and 21, 22 to something better
#everyone must improve these lines
	

'''
for name in range(len(names)):
		if name in names_dict.keys:
			names_dict[name] += 1
		else:
			names_dict[name] = 1
'''
'''
Test 1
Input: names: ["doc", "doc", "image", "doc(1)", "doc"]
Expected Output: ["doc", "doc(1)", "image", "doc(1)(1)", "doc(2)"]

Test 2
Input: 
names: ["a(1)", "a(6)", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a"]
Expected Output:
["a(1)", "a(6)", "a", "a(2)", "a(3)", "a(4)", "a(5)", "a(7)", "a(8)", 
"a(9)", "a(10)", "a(11)"]

Test 3
Input:
names: ["dd", "dd(1)", "dd(2)", "dd", "dd(1)", "dd(1)(2)", "dd(1)(1)", "dd",
"dd(1)"]
Expected Output:
["dd", "dd(1)", "dd(2)", "dd(3)", "dd(1)(1)", "dd(1)(2)", "dd(1)(1)(1)",
"dd(4)", "dd(1)(3)"]
'''